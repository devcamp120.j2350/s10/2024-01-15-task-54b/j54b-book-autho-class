import model.Author;
import model.Book;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Author author1 = new Author("Nguyen Van Nam", "namnv@gmail.com", 'm');
        System.out.println(author1);

        Author author2 = new Author("Le Tuyet Minh", "minhlt@gmail.com", 'f');
        System.out.println(author2);

        Book book1 = new Book("Java", 300000, author1);
        System.out.println(book1);

        Book book2 = new Book("HTML", 150000, 5, author2);
        System.out.println(book2);
    }
}
